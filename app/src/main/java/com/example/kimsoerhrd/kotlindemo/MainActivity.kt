package com.example.kimsoerhrd.kotlindemo

import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.webkit.ValueCallback
import android.webkit.WebView
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        webview.loadUrl("https://www.google.com")
        webview.settings.javaScriptEnabled = true
        webview.webViewClient = WebViewClient()
        var data:String ="javascript: document.getElementsByClassName('gLFyf')[0].value= 'test'";


        webview.setWebViewClient(object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)

                if (Build.VERSION.SDK_INT >= 19) {
                    view.evaluateJavascript(data) { }
                } else {
                    view.loadUrl(data)
                }
            }

        })


    }


}
